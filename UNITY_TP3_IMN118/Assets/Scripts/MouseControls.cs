﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControls : MonoBehaviour
{
    public Transform board;
    public Vector2 lastMousePos;
    public float rotationSpeed = 10.0f;
    public float maxRotation = 35f;

    private void Start()
    {
        lastMousePos = new Vector2(0, 0);
    }


    void FixedUpdate()
    {
        if(Input.GetMouseButtonDown(0))
        {
            lastMousePos = Input.mousePosition;
        }
        if(Input.GetMouseButton(0))
        {
            Vector2 deltaMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - lastMousePos;
            float v = Mathf.Clamp(board.transform.localRotation.y + Time.deltaTime * rotationSpeed * deltaMousePos.y, -maxRotation, maxRotation);
            float h = Mathf.Clamp(board.transform.localRotation.x + Time.deltaTime * rotationSpeed * deltaMousePos.x, -maxRotation, maxRotation);

            board.transform.localRotation = Quaternion.Euler(v, 0, -h);
        }
    }
}
