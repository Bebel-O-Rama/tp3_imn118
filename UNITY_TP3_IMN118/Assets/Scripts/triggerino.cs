﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerino : MonoBehaviour
{
    public Collider playerCollider;
    public BoxCollider bcBase;
    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            bcBase.enabled = false;
        }
    }
}
