﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nice : MonoBehaviour
{
    public Collider playerCollider;
    public AudioClip nicee;
    public AudioSource SourceOfNice;
    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            SourceOfNice.PlayOneShot(nicee, 1);
        }
    }
}
