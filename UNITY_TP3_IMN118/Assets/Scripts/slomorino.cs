﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slomorino : MonoBehaviour
{
    public Collider playerCollider;
    public BoxCollider bcBase;
    public AudioClip sadness;
    public AudioSource SourceOfSadness;
    public AudioSource OST;
    public Collider oofBox;
    public bool isSad;
    public GameObject backGroundObject;
    public GameObject mainCam;
    public GameObject sadCam;
    private void Start()
    {
        isSad = false;
        backGroundObject = GameObject.Find("NiceBackGround");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            bcBase.enabled = false;
            int rdmDeath = Random.Range(0, 9);
            Debug.Log(rdmDeath);
            if (rdmDeath < 5)
            {
                mainCam.SetActive(false);
                sadCam.SetActive(true);
                backGroundObject.GetComponent<UnityEngine.Video.VideoPlayer>().playbackSpeed = 0.3f;
                Time.timeScale = 0.3f;
                OST.enabled = false;
                oofBox.enabled = false;
                SourceOfSadness.PlayOneShot(sadness, 1);
            }
        }
    }
}
