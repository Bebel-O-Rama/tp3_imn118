﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ooferino : MonoBehaviour

{
    public Collider playerCollider;
    public AudioClip oof;
    public AudioSource SourceOfOof;
    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            SourceOfOof.PlayOneShot(oof, 1);
        }
    }
}
